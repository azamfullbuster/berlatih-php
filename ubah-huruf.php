<?php
function ubah_huruf($string)
{
    $change = [];
    for ($i = 0; $i < strlen($string); $i++) {
        $ascii = ord($string[$i]);
        $change[] = chr($ascii + 1);
    }
    $change_str = implode($change);
    return $change_str;
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo nl2br("\n");
echo ubah_huruf('developer'); // efwfmpqfs
echo nl2br("\n");
echo ubah_huruf('laravel'); // mbsbwfm
echo nl2br("\n");
echo ubah_huruf('keren'); // lfsfo
echo nl2br("\n");
echo ubah_huruf('semangat'); // tfnbohbu
