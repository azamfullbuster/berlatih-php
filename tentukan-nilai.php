<?php
function tentukan_nilai($number)
{
    $grade = "";
    if ($number >= 85 && $number <= 100) {
        $grade = "Sangat Baik";
    } elseif ($number >= 70 && $number < 85) {
        $grade = "Baik";
    } elseif ($number >= 60 && $number < 70) {
        $grade = "Cukup";
    } else {
        $grade = "Kurang";
    }
    return $grade;
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo nl2br("\n");
echo tentukan_nilai(76); //Baik
echo nl2br("\n");
echo tentukan_nilai(67); //Cukup
echo nl2br("\n");
echo tentukan_nilai(43); //Kurang
